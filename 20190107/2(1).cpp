#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <vector>

int twod_to_oned(int row, int col, int rowlen){
    return row*rowlen+col;
}

int mfind(int n, const std::vector<int>& v){
    // write your code here

    for (int i = n ; i >= 0 ; i = i - 1){
      if(n == v[i]){
        return 1;
      }
    }
        return -1;
      }

      bool check_sequence(const std::vector<int>& v){
          // write your code here
          int n, m;
          n = v.size();
          for (int i = n; i > 0; i = i - 1)
          {
            m = mfind(i, v);
            if(m == -1)
            {
              return false;
            }
          }
        return true;
      }

      void get_row(int r, const std::vector<int>& in, std::vector<int>& out){
          // write your code here
          int n;
          n = std::sqrt(in.size());
          for (int i = 0; i < n; i = i +1){
            int m = twod_to_oned(r, i, n);
            out.push_back(in[m]);
          }
      }

      void get_subsq(int subs, const std::vector<int>& in, std::vector<int>& out){
          // write your code here
          int n, row, col;
          n = std::sqrt(std::sqrt(in.size()));
          row = n*(subs%n);
          col = n*(subs - row);
          //i representing rows
          for (int i = 0; i < n; i = i +1)
          {
            //j representing columns
            for (int j = 0; j < n; j = j + 1)
            {
              out.push_back(in[twod_to_oned(row + i , col + j , n*n)]);
            }
          }
      }


int main(){
  std::vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(3);
  v.push_back(4);
  v.push_back(5);
  v.push_back(6);
  v.push_back(7);
  v.push_back(8);
  v.push_back(9);
  v.push_back(10);
  v.push_back(11);
  v.push_back(12);
  v.push_back(13);
  v.push_back(14);
  v.push_back(15);
  v.push_back(16);


  //for(int i = 0; i << v.size() ; i = i + 1){
  //std::cout<< v[i] <<std::endl;
//};
std::cout<< v[0] <<"|"<< v[1] << "|"<< v[2] <<std::endl;

  int n, m;
  n = mfind(-2, v);
  std::cout<< n <<std::endl;
  m = check_sequence(v);
  std::cout<< m <<std::endl;
  std::vector<int> out;
  get_subsq(1, v, out);
  std::cout<<out[0]<<"|"<<out[1]<<"|"<<out[2]<<"|"<<out[3]<<std::endl;
  return 0;
}
