#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <vector>

bool valid_sudoku(const std::vector<int> &g);
bool check_sequence(const std::vector<int> &v);
int mfind(int n, const std::vector<int> &v);

void get_row(int r, const std::vector<int> &in, std::vector<int> &out);
void get_col(int c, const std::vector<int> &in, std::vector<int> &out);
void get_subsq(int subs, const std::vector<int> &in, std::vector<int> &out);

int twod_to_oned(int row, int col, int rowlen);

int main()
{

    std::vector<int> s;

    std::string filename;

    std::cout << "please enter name of file containing the sudoku" << std::endl;
    std::cin >> filename;

    std::ifstream infile;
    infile.open(filename.c_str());

    if (!infile.is_open())
    {
        std::cout << "error, can't open input file" << std::endl;
        exit(EXIT_FAILURE);
    }

    int tmp;

    while (infile >> tmp)
    {
        s.push_back(tmp);
    }

    int side = std::sqrt(s.size());

    for (int i = 0; i < side; i++)
    {
        for (int j = 0; j < side; j++)
        {
            std::cout << s[twod_to_oned(i, j, side)] << " ";
        }
        std::cout << std::endl;
    }

    // calling the function checking if the sudoku is a valid one:

    bool valid = valid_sudoku(s);

    if (valid)
    {
        std::cout << "valid" << std::endl;
    }
    else
    {
        std::cout << "not valid" << std::endl;
    }

    return 0;
}

int twod_to_oned(int row, int col, int rowlen)
{
    return row * rowlen + col;
}

bool valid_sudoku(const std::vector<int> &g)
{
    int side = std::sqrt(g.size());

    // for each row...
    for (int i = 0; i < side; i++)
    {
        std::vector<int> row;
        get_row(i, g, row);

        if (!check_sequence(row))
        {
            // std::cout << "#" <<i <<" row check failed" << std::endl;
            return false;
        }
        // std::cout << "#" <<i <<" row check ok" << std::endl;
    }

    // for each column...
    for (int i = 0; i < side; i++)
    {
        // write your code here
        std::vector<int> col;
        get_col(i, g, col);

        if (!check_sequence(col))
        {
            // std::cout << "#" <<i <<" col check failed" << std::endl;
            return false;
        }
        // std::cout << "#" <<i <<" col check ok" << std::endl;
    }

    // for each subsquare...
    for (int i = 0; i < side; i++)
    {
        // write your code here
        std::vector<int> subsq;
        get_subsq(i, g, subsq);

        if (!check_sequence(subsq))
        {
            // std::cout << "#" <<i <<" subsq check failed" << std::endl;
            return false;
        }
        // std::cout << "#" <<i <<" subsq check ok" << std::endl;
    }
    return true;
}

// function mfind
// takes in input:
// - a vector of integers v
// - an integer n
// returns:
// the index of the element n in v, if n is in v
// -1 if n is not in v
// for instance:
// v: 3 2 5; n: 2 | return: 1
// v: 3 1 2; n: -6 | return: -1
// v: 1 1 -12 1 5; n: 5 | return: 4

int mfind(int n, const std::vector<int> &v)
{
    // write your code here
    for (int i = v.size() - 1; i >= 0; i = i - 1)
    {
        // std::cout << "n=" << n << "; i=" << i << "; v[i]=" << v[i] << std::endl;
        if (n == v[i])
        {
            return 1;
        }
    }
    return -1;
}

bool check_sequence(const std::vector<int> &v)
{
    // write your code here
    int n, m;
    n = v.size();
    for (int i = n; i > 0; i = i - 1)
    {
        m = mfind(i, v);
        // std::cout << "i=" << i << "; m=" <<m << std::endl;
        if (m == -1)
        {
            return false;
        }
    }
    return true;
}

void get_row(int r, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    int n;
    n = std::sqrt(in.size());
    for (int i = 0; i < n; i = i + 1)
    {
        int m = twod_to_oned(r, i, n);
        out.push_back(in[m]);
    }
}

void get_col(int c, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    int n;
    n = std::sqrt(in.size());
    for (int i = 0; i < n; i = i + 1)
    {
        int m = twod_to_oned(i, c, n);
        out.push_back(in[m]);
    }
}

// function get_subsq
// like the two functions above but for subsquares
// we consider subsquares to be indexed from left to right
// and then from top to bottom
// for example:
// subs: 0; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 1 2 4 1
// subs: 1; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 3 4 2 76
// subs: 2; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 4 4 9 4
// subs: 3; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 4 1 1 -7
// void get_subsq(int subs, const std::vector<int> &in, std::vector<int> &out)
// {
//     // write your code here
//     int n, m, l;
//     n = std::sqrt(std::sqrt(in.size()));
//     m = subs % n;
//     l = (subs - m) / n;
//     //i representing rolls
//     for (int i = 0; i < n; i = i + 1)
//     {
//         //j representing columns
//         for (int j = 0; j < n; j = j + 1)
//         {
//             out.push_back(in[twod_to_oned(3 * l + i, 3 * m + j, n * n)]);
//         }
//     }
// }

void get_subsq(int subs, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    // out.clear();
    int n, row, col;
    n = std::sqrt(std::sqrt(in.size()));
    col = n * (subs % n);
    row = n * int(subs / n);
    //   std::cout << "row=" << row << "col=" << col <<std::endl;
    //i representing rows
    for (int i = 0; i < n; i = i + 1)
    {
        //j representing columns
        for (int j = 0; j < n; j = j + 1)
        {
            // std::cout << "y=" << row+i << "x=" << col+j << ":" << in[(row+i)*n*n+col+j] <<std::endl;
            // out.push_back(in[(row + i) * n * n + col + j]);
            out.push_back(in[twod_to_oned(row + i, col + j, n * n)]);
        }
    }
}

void printv(std::vector<int> v)
{

    int n = std::sqrt(v.size());

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            std::cout << v[i * n + j] << "\t";
        }
        std::cout << std::endl;
    }
}

// int main(){
//     std::vector<int> v;
//     v.push_back(1);
//     v.push_back(2);
//     v.push_back(3);
//     v.push_back(4);
//     v.push_back(5);
//     v.push_back(6);
//     v.push_back(7);
//     v.push_back(8);
//     v.push_back(9);
//     v.push_back(10);
//     v.push_back(11);
//     v.push_back(12);
//     v.push_back(13);
//     v.push_back(14);
//     v.push_back(15);
//     v.push_back(16);

//     std::cout << "v=" <<std::endl;
//     printv(v);
//     std::cout << std::endl;

//     std::vector<int> sub;
//     for (int i = 0; i < std::sqrt(v.size()); i++){
//         std::cout << "sub=" <<std::endl;
//         sub.clear();
//         get_subsq(i, v, sub);
//         printv( sub );
//         std::cout << std::endl;
//     }

// }
