#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <vector>

bool valid_sudoku(const std::vector<int> &g);
bool check_sequence(const std::vector<int> &v);
int mfind(int n, const std::vector<int> &v);

void get_row(int r, const std::vector<int> &in, std::vector<int> &out);
void get_col(int c, const std::vector<int> &in, std::vector<int> &out);
void get_subsq(int subs, const std::vector<int> &in, std::vector<int> &out);

int twod_to_oned(int row, int col, int rowlen);

int main()
{

    std::vector<int> s;

    std::string filename;

    std::cout << "please enter name of file containing the sudoku" << std::endl;
    std::cin >> filename;

    std::ifstream infile;
    infile.open(filename.c_str());

    if (!infile.is_open())
    {
        std::cout << "error, can't open input file" << std::endl;
        exit(EXIT_FAILURE);
    }

    int tmp;

    while (infile >> tmp)
    {
        s.push_back(tmp);
    }

    int side = std::sqrt(s.size());

    for (int i = 0; i < side; i++)
    {
        for (int j = 0; j < side; j++)
        {
            std::cout << s[twod_to_oned(i, j, side)] << " ";
        }
        std::cout << std::endl;
    }

    // calling the function checking if the sudoku is a valid one:

    bool valid = valid_sudoku(s);

    if (valid)
    {
        std::cout << "valid" << std::endl;
    }
    else
    {
        std::cout << "not valid" << std::endl;
    }

    return 0;
}

int twod_to_oned(int row, int col, int rowlen)
{
    return row * rowlen + col;
}

bool valid_sudoku(const std::vector<int> &g)
{
    int side = std::sqrt(g.size());

    // for each row...
    for (int i = 0; i < side; i++)
    {
        std::vector<int> row;
        get_row(i, g, row);

        if (!check_sequence(row))
        {
            return false;
        }
    }

    // for each column...
    for (int i = 0; i < side; i++)
    {
        // write your code here
        std::vector<int> col;
        get_col(i, g, col);

        if (!check_sequence(col))
        {
            return false;
        }
    }

    // for each subsquare...
    for (int i = 0; i < side; i++)
    {
        // write your code here
        std::vector<int> subsq;
        get_subsq(i, g, subsq);

        if (!check_sequence(subsq))
        {
            return false;
        }
    }
    return true;
}

// function mfind
// takes in input:
// - a vector of integers v
// - an integer n
// returns:
// the index of the element n in v, if n is in v
// -1 if n is not in v
// for instance:
// v: 3 2 5; n: 2 | return: 1
// v: 3 1 2; n: -6 | return: -1
// v: 1 1 -12 1 5; n: 5 | return: 4

int mfind(int n, const std::vector<int> &v)
{
    // write your code here
    for (int i = v.size() - 1; i >= 0; i = i - 1)
    {
        if (n == v[i])
        {
            return 1;
        }
    }
    return -1;
}

bool check_sequence(const std::vector<int> &v)
{
    // write your code here
    int n, m;
    n = v.size();
    for (int i = n - 1; i >= 0; i = i - 1)
    {
        m = mfind(i, v);
        if (m == -1)
        {
            return false;
        }
    }
    return true;
}

void get_row(int r, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    int n;
    n = std::sqrt(in.size());
    for (int i = 0; i < n; i = i + 1)
    {
        int m = twod_to_oned(r, i, n);
        out.push_back(in[m]);
    }
}

void get_col(int c, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    int n;
    n = std::sqrt(in.size());
    for (int i = 0; i < n; i = i + 1)
    {
        int m = twod_to_oned(i, c, n);
        out.push_back(in[m]);
    }
}

// function get_subsq
// like the two functions above but for subsquares
// we consider subsquares to be indexed from left to right
// and then from top to bottom
// for example:
// subs: 0; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 1 2 4 1
// subs: 1; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 3 4 2 76
// subs: 2; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 4 4 9 4
// subs: 3; in: 1 2 3 4 4 1 2 76 4 4 4 1 9 4 1 -7 | out: 4 1 1 -7
void get_subsq(int subs, const std::vector<int> &in, std::vector<int> &out)
{
    // write your code here
    int n, row, col;
    n = std::sqrt(std::sqrt(in.size()));
    col = n * (subs % n);
    row = n * int(subs / n);
    //i representing rows
    for (int i = 0; i < n; i = i + 1)
    {
        //j representing columns
        for (int j = 0; j < n; j = j + 1)
        {
            out.push_back(in[twod_to_oned(row + i, col + j, n * n)]);
        }
    }
}
