#include <iostream>
#include <cmath>
struct point
{
  double x;
  double y;
};
double perimeter(point p1, point p2, point p3);

int main()
{
  point pa, pb, pc;
  std::cout << "input the first point:" << std::endl;
  std::cin >> pa.x >> pa.y;
  std::cout << "input the second point:" << std::endl;
  std::cin >> pb.x >> pb.y;
  std::cout << "input the third point:" << std::endl;
  std::cin >> pc.x >> pc.y;
  std::cout << "the perimeter is: " << perimeter(pa, pb, pc) << std::endl;
  return 0;
}

double perimeter(point p1, point p2, point p3)
{
  double d1, d2, d3;
  d1 = std::sqrt(std::pow((p1.x - p2.x), 2) + std::pow((p1.y - p2.y), 2));
  d2 = std::sqrt(std::pow((p1.x - p3.x), 2) + std::pow((p1.y - p3.y), 2));
  d3 = std::sqrt(std::pow((p3.x - p2.x), 2) + std::pow((p3.y - p2.y), 2));
  return (d1 + d2 + d3);
}
