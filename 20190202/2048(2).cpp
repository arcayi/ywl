#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <vector>
//#include <random>
#include <cstdlib>

void rotate_anti_clock(std::vector<int>& v);
int twod_to_oned(int row, int col, int rowlen);
bool proc_num(std::vector<int>& v, int bi, int ei);
bool game_over(const std::vector<int>& v);
void print_grid(const std::vector<int>& v);
void rand_zero(std::vector<int>& v);


int main() {
  std::string filename;
  std::vector<int> v, zero;
  std::string x;

  std::cout << "enter initial configure file name" << std::endl;
  std::cin >> filename;

  std::ifstream infile;
  infile.open(filename.c_str());

  if(!infile.is_open()){
    for(int i=0; i < 15; i ++){
      v.push_back(0);
    }
    v.push_back(2);
  }

  else {
  int tmp;
  while(infile >> tmp){
      v.push_back(tmp);
    }
  }

  int side = std::sqrt(v.size());

bool gameover;
gameover = game_over(v);
if (gameover != true){
  print_grid(v);
}

while(gameover == 0){
  std::cin>> x;
  bool change = 0;
  if(x == "a"){
    for (int row = 0; row < v.size(); row = row + side){
      if(proc_num(v, row, row+side) == 1){
        change = 1;
      }
        //proc_num(v, twod_to_oned(row, 0, side), twod_to_oned(row, side, side))
    }
    if(change == 1){
      rand_zero(v);
    }
    print_grid(v);
  }
  if(x == "w"){
    rotate_anti_clock(v);
    for (int row = 0; row < v.size(); row = row + side){
      if(proc_num(v, row, row+side) == 1){
        change = 1;
      }
    }
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    if(change == 1){
      rand_zero(v);
    }
    print_grid(v);
  }
  if(x == "d"){
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    for (int row = 0; row < v.size(); row = row + side){
      if(proc_num(v, row, row+side) == 1){
        change = 1;
      }
    }
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    if(change == 1){
      rand_zero(v);
    }
    print_grid(v);
  }
  if(x == "s"){
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    rotate_anti_clock(v);
    for (int row = 0; row < v.size(); row = row + side){
      if(proc_num(v, row, row+side) == 1){
        change = 1;
      }
    }
    rotate_anti_clock(v);
    if(change == 1){
      rand_zero(v);
    }
    print_grid(v);
  }
  gameover = game_over(v);
}
print_grid(v);
std::cout<<"gameover"<< std::endl;
  return 0;
}

bool proc_num(std::vector<int>& v, int bi, int ei){
  int i = bi;
  int k = bi;
  bool gap=0;
  bool addition=0;
  for(int g=bi; g<ei-1;g++)
  {
    if(v[g+1]!=0 && v[g]==0)
    {
      gap=1;
    }
  }
  while (i < ei){

    if(v[i] == 0){
      i = i + 1;
    }
    else{
      for (int j = i+1 ; j < ei ; j++)
      {

        if(v[i] == v[j]){
          v[k] = v[i] + v[j];
          addition=1;
          k = k + 1;
          i = j + 1;
          j=ei;

        }
        if(v[i]!=v[j] &&v[j]!=0 && j!=ei){
          v[k] = v[i];
          k = k + 1;
          i = j;
          j=ei;
        }
    if (j==ei-1 && v[j]==0)
                {v[k]=v[i];
                k=k+1;
                j=ei;
                i=ei;
                }
    }

    if(i==ei-1)
      {
      v[k]=v[i];
      k=k+1;
      i=ei;
    }
      }
    }

  while( k < ei ){
      v[k] = 0;
      k = k + 1;
    }
  if(gap==1){return 1;}
  if(addition==1){return 1;}
    return 0;
 }
void rotate_anti_clock(std::vector<int>& v){
   std::vector<int> b;
   int side = std::sqrt(v.size());
   //i is row number,
   //j is column number.
   for (int j = side-1; j >= 0; j = j - 1){
     for(int i = 0; i < side; i++){
       b.push_back(v[twod_to_oned(i, j, side)]);
     }
   }
   for (int k = 0; k < v.size(); k++){
     v[k] = b[k];
   }
 }
bool game_over(const std::vector<int>& v){
   std::vector<int> b;
   for(int i=0; i < v.size(); i++){
     b.push_back(v[i]);
   }
   int side = std::sqrt(b.size());
   bool furtherchange = 0;
   bool row = 0;
   bool col = 0;
   for (int i = 0; i < side; i = i +side){
     if(proc_num(b, row, row+side) == 1){
       furtherchange = 1;
     }
   }
   rotate_anti_clock(b);
   for (int i = 0; i < side; i = i +side){
     if(proc_num(b, row, row+side) == 1){
       furtherchange = 1;
     }
   }
    rotate_anti_clock(b);
    for (int i = 0; i < side; i = i +side){
      if(proc_num(b, row, row+side) == 1){
        furtherchange = 1;
      }
    }
    rotate_anti_clock(b);
    for (int i = 0; i < side; i = i +side){
      if(proc_num(b, row, row+side) == 1){
        furtherchange = 1;
      }
    }
    if(furtherchange == 1){
      return false;
    }
    else {
      return true;
   }
 }
void print_grid(const std::vector<int>& v){
 int side = std::sqrt(v.size());
 for(int i = 0; i < side; i++){
   for(int j = 0; j < side; j++){
     std::cout<< v[twod_to_oned(i, j, side)] << "\t"  ;
   }
   std::cout<<std::endl;
 }
 }
int twod_to_oned(int row, int col, int rowlen){
   return row*rowlen+col;
 }
void rand_zero(std::vector<int>& v){
  std::vector<int> b;
  for(int i = 0; i < v.size(); i++){
    if(v[i] == 0){
      b.push_back(i);
    }
  }
  v[b[rand()%(b.size())]] = 2;
}
