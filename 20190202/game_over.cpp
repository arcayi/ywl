#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

bool proc_num(std::vector<int>& v, int bi, int ei);
int twod_to_oned(int row, int col, int rowlen);
bool game_over(const std::vector<int>& v);
void get_col(int c, std::vector<int>& in, std::vector<int>& out);
void rotate_anti_clock(std::vector<int>& v);
void print_grid(const std::vector<int>& v);

int main()
{
  std::string filename;
  std::vector<int> v;

  std::cout << "enter initial configure file name" << std::endl;
  std::cin >> filename;

  std::ifstream infile;
  infile.open(filename.c_str());

  if (!infile.is_open())
  {
    std::cout << "error, can't open input file" << std::endl;
    exit(EXIT_FAILURE);
  }

  int tmp;

  while (infile >> tmp)
  {
    v.push_back(tmp);
  }

  // int bi, ei;
  // std::cout << "enter the bi and ei" << std::endl;
  // std::cin >> bi >> ei;
  // bool change;
  // change = proc_num(v, bi, ei);
  // if (!change)
  // {
  //   std::cout << "true" << std::endl;
  // }
  // else
  // {
  //   std::cout << "false" << std::endl;
  // }

  print_grid(v);
  std::cout << "var gameover=" << game_over(v) << std::endl;

  return 0;
}

bool game_over(const std::vector<int>& v)
{
  std::vector<int> b;
  for (int i = 0; i < v.size(); i++)
  {
    b.push_back(v[i]);
  }
  int side = std::sqrt(b.size());
  bool furtherchange = 0;
  bool row = 0;
  bool col = 0;
  for (int i = 0; i < side; i = i + side)
  {
    if (proc_num(b, row, row + side) == 1)
    {
      furtherchange = 1;
    }
  }
  rotate_anti_clock(b);
  for (int i = 0; i < side; i = i + side)
  {
    if (proc_num(b, row, row + side) == 1)
    {
      furtherchange = 1;
    }
  }
  rotate_anti_clock(b);
  for (int i = 0; i < side; i = i + side)
  {
    if (proc_num(b, row, row + side) == 1)
    {
      furtherchange = 1;
    }
  }
  rotate_anti_clock(b);
  for (int i = 0; i < side; i = i + side)
  {
    if (proc_num(b, row, row + side) == 1)
    {
      furtherchange = 1;
    }
  }
  // if (furtherchange = 1)
  if (1 == furtherchange)
  {
    return false;
  }
  else
  {
    return true;
  }
}
/* std::vector<int> col;
for (int c = 0; c < side; c++){
int i = c*side;
get_col(c, v, col);
//for (int i = 0; i < side; i++){
  furtherchange = proc_num(col, i, i+side);
  if (furtherchange){
    return true;
  }
//}
} */

bool proc_num(std::vector<int>& v, int bi, int ei)
{
  // std::vector<int> vout;
  int i = bi;
  int k = bi;
  bool gap = 0;
  bool addition = 0;
  for (int g = bi; g < ei - 1; g++)
  {
    if (v[g + 1] != 0 && v[g] == 0)
    {
      gap = 1;
    }
  }
  while (i < ei)
  {
    if (v[i] == 0)
    {
      i = i + 1;
    }
    // skip all v[i] = 0
    else
    {
      for (int j = i + 1; j < ei; j++)
      {
        // when v[i] != 0, skip all v[j] = 0

        if (v[i] == v[j])
        {
          // when v[i] and v[j] are both none zero and equal
          // save the value of v[i] + v[j]
          // v[i] = v[i] + v[j];
          v[k] = v[i] + v[j];
          addition = 1;
          k = k + 1;
          // v[j] = 0;
          i = j + 1;
          j = ei;

          // since v[j] has undergo one operation, it cannot be added to the
          // element after it again, continu to v[j+1]
        }
        if (v[i] != v[j] && v[j] != 0 && j != ei)
        {
          // when v[i] and v[j] are both none zero but not equal
          // save the value of v[i]
          v[k] = v[i];
          k = k + 1;
          i = j;
          j = ei;

          // since v[j] has not been add to the element before it, continu with
          // v[j] to see it it can be added to the nunmber behind it.
        }
        if (j == ei - 1 && v[j] == 0)
        {
          v[k] = v[i];
          k = k + 1;
          j = ei;
          i = ei;
        }
      }

      if (i == ei - 1)
      {
        v[k] = v[i];
        k = k + 1;
        i = ei;
      }
      // i = i + 1;
    }
    //在i是ei-1时进不去j循环；i就一直是ei-1导致死循环！！！！
  }

  while (k < ei)
  {
    //这个while也可以不要吧？ 当然不能了伊雯琳大傻子你不介意我把你名字打错的吧。
    v[k] = 0;
    k = k + 1;
  }
  if (gap == 1)
  {
    return 1;
  }
  if (addition == 1)
  {
    return 1;
  }
  return 0;

  /*  if(vout.size() >= (ei - bi)){
      return 0;
    }
    else{
      return 1;
    } */
}

int twod_to_oned(int row, int col, int rowlen)
{
  return row * rowlen + col;
}

void get_col(int c, const std::vector<int>& in, std::vector<int>& out)
{
  // write your code here
  int n;
  n = std::sqrt(in.size());
  for (int i = 0; i < n; i = i + 1)
  {
    int m = twod_to_oned(i, c, n);
    out.push_back(in[m]);
  }
}

void rotate_anti_clock(std::vector<int>& v)
{
  std::vector<int> b;
  int side = std::sqrt(v.size());
  // i is row number,
  // j is column number.
  for (int j = side - 1; j >= 0; j = j - 1)
  {
    for (int i = 0; i < side; i++)
    {
      b.push_back(v[twod_to_oned(i, j, side)]);
    }
  }
  for (int k = 0; k < v.size(); k++)
  {
    v[k] = b[k];
  }
}

void print_grid(const std::vector<int>& v)
{
  int side = std::sqrt(v.size());
  for (int i = 0; i < side; i++)
  {
    for (int j = 0; j < side; j++)
    {
      std::cout << v[twod_to_oned(j, i, side)] << "\t";
    }
    std::cout << std::endl;
  }
}