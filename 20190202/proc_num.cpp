#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

bool proc_num(std::vector<int> &v, int bi, int ei);

void printv(std::vector<int> &v) {
  for (int i_subst = 0; i_subst < v.size(); i_subst++) {
    std::cout << v[i_subst] << " ";
  }
  std::cout << std::endl;
}

int main() {
  std::string filename;
  std::vector<int> v;

  std::cout << "enter initial configure file name" << std::endl;
  std::cin >> filename;

  std::ifstream infile;
  infile.open(filename.c_str());

  if (!infile.is_open()) {
    std::cout << "error, can't open input file" << std::endl;
    exit(EXIT_FAILURE);
  }

  int tmp;

  while (infile >> tmp) {
    v.push_back(tmp);
  }

  std::cout << "v before:";
  printv(v);

  int bi, ei;
  std::cout << "enter the bi and ei" << std::endl;
  std::cin >> bi >> ei;

  bool change;

  change = proc_num(v, bi, ei);
  std::cout << change << std::endl;
  if (!change) {
    std::cout << "false" << std::endl;
  } else {
    std::cout << "true" << std::endl;
  }

  std::cout << "v after:";
  printv(v);

  return 0;
}

bool proc_num(std::vector<int> &v, int bi, int ei) {
  bool is_found = false;
  for (int i_cur = bi, i_subst = bi; i_subst < ei;
       i_subst++) { // i_cur for output
    std::cout << ""
              << "i_cur:" << i_cur << "\t"
              << "i_subst:" << i_subst << "\t";
    std::cout << "v[" << i_cur << "]:" << v[i_cur] << "\t"
              << "v[" << i_subst << "]:" << v[i_subst] << "\t" << std::endl;
    if (v[i_subst] != 0) { // skip all v[i_subst] = 0
      for (int j_search = i_subst + 1; j_search < ei; j_search++) {
        std::cout << "\t\t"
                  << "j_search:" << j_search << "\t";
        std::cout << ""
                  << "v[" << j_search << "]:" << v[j_search] << "\t";
        if (0 != v[j_search]) { // skip all v[j_search] = 0
          std::cout << "non-zero"
                    << "\t";
          if (v[i_subst] == v[j_search]) {
            // when v[i_subst] and v[j_search] are both none zero
            // and equal save the value of v[i_subst] + v[j_search]
            std::cout << "found"
                      << "\t";
            v[i_subst] = v[i_subst] * 2;
            v[j_search] = 0;
            is_found = true;
            // since v[j_search] has undergo one operation, it
            // cannot be added to the element after it again,
            // continu to v[j_search+1]
          }
          // i_subst = j_search;
          std::cout << std::endl;
          break;
        }
        std::cout << std::endl;
      }
      int temp = v[i_subst];
      v[i_subst] = 0;
      v[i_cur] = temp;
      i_cur++;
    }
  }
  return (is_found);
}
